package question3;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	
	public static Collection<Planet> getLargerThan(Collection<Planet> planets,double size)
	{
		ArrayList<Planet> planet = new ArrayList<>();
		for(Planet p : planets)
		{
			if(p.getRadius()>=size)
			{
				planet.add(p);
			}
		}
		
		return planet;
		
	}

}
