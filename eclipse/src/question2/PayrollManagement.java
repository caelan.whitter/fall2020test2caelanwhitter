package question2;

public class PayrollManagement {

	public static void main(String[] args) {
		Employee [] emps = new Employee [5];
		emps[0]= new SalariedEmployee(10000);
		emps[1]= new HourlyEmployee(40,17);
		emps[2]= new UnionizedHourlyEmployee(30,18,30,1.5);
		emps[3]= new SalariedEmployee(20000);
		emps[4]= new SalariedEmployee(17000);
		
		System.out.println(getTotalExpenses(emps));

	}
	
	public static double getTotalExpenses(Employee[] emps)
	{
		double money = 0;
		for( Employee emp : emps)
		{
			money+= emp.getWeeklyPay();
		}
		return money;
	}

}
