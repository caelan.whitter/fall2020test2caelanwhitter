package question2;



public class SalariedEmployee implements Employee {
	
	private double salary;
	
	
	public double getWeeklyPay() {
	
		double yearlySalary = this.getSalary() / 52;
		return yearlySalary;
		
	}
	
	public SalariedEmployee(double salary) 
	{
		this.salary=salary;
	}
	
	public double getSalary()
	{
		return this.salary;
	}


	
	

}
