package question2;

public class HourlyEmployee implements Employee{
	
	private double numberHours;
	private double hourlyPay;
	
	
	public double getWeeklyPay() {

		double weeklyPay = this.getHourlyPay() * this.getHours();
		return weeklyPay;
	}
	
	public HourlyEmployee(double numberHours,double hourlyPay)
	{
		this.numberHours = numberHours;
		this.hourlyPay = hourlyPay;
	}
	
	public double getHours()
	{
		return this.numberHours;
	}
	public double getHourlyPay()
	{
		return this.hourlyPay;
	}

}
