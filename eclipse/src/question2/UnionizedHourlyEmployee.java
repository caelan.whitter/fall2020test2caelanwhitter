package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double numberHours, double hourlyPay,double maxHoursPerWeek,double overtimeRate) 
	{
		super(numberHours, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	
	public double getWeeklyPay()
	{
		
		double newPay = 0;

		if(getHours()<=this.maxHoursPerWeek)
		{
			 newPay = getHourlyPay() * getHours();
		}
		if(getHours()>this.maxHoursPerWeek)
		{
			double additionalHours = getHours()-this.maxHoursPerWeek;
			double normal = getHours() * getHourlyPay();
			double overtime = additionalHours * this.overtimeRate;
			newPay = normal + overtime;
		}
		
		
		return newPay;
	
	}
}

